# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Frank Weng (a.k.a. Franklin) <franklin at goodhorse dot idv dot tw>, 2008, 2009, 2013.
# pan93412 <pan93412@gmail.com>, 2018.
msgid ""
msgstr ""
"Project-Id-Version: kdevkonsole\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2021-11-09 00:15+0000\n"
"PO-Revision-Date: 2018-12-02 21:11+0800\n"
"Last-Translator: pan93412 <pan93412@gmail.com>\n"
"Language-Team: Chinese <zh-l10n@lists.linux.org.tw>\n"
"Language: zh_TW\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"dot tw>\n"
"X-Generator: Lokalize 2.0\n"
"Plural-Forms: nplurals=1; plural=0;\n"

#: kdevkonsoleview.cpp:107
#, kde-format
msgid "Terminal"
msgstr ""

#: kdevkonsoleview.cpp:110 kdevkonsoleviewplugin.cpp:62
#, kde-format
msgctxt "@title:window"
msgid "Terminal"
msgstr ""

#: kdevkonsoleviewplugin.cpp:60
#, kde-format
msgid "Failed to load 'konsolepart' plugin"
msgstr "無法載入「konsolepart」外掛程式"

#~ msgid "Konsole"
#~ msgstr "Konsole"

#, fuzzy
#~| msgid "Konsole"
#~ msgctxt "@title:window"
#~ msgid "Konsole"
#~ msgstr "Konsole"
